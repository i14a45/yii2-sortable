Yii2 Sortable Behavior
=======================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
```
composer require i14a45/yii2-sortable "dev-master"
```
