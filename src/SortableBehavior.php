<?php


namespace i14a45\sortable;


use yii\base\Behavior;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property ActiveRecord $owner
 */
class SortableBehavior extends Behavior
{
    /**
     * @var string
     */
    public $positionAttribute = 'position';

    /**
     * @var array
     */
    public $groupAttributes = [];

    /**
     * @var int
     */
    public $step = 10;

    /**
     * {@inheritDoc}
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
        ];
    }

    /**
     *
     */
    public function beforeInsert()
    {
        $this->owner->setAttribute($this->positionAttribute, $this->getLastPosition() + $this->step);
    }

    /**
     * @return int
     */
    protected function getLastPosition()
    {
        return (int) $this->getQuery()->max($this->positionAttribute);
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $attributeValues = $this->owner->getAttributes($this->groupAttributes);
        $condition = array_combine(array_keys($attributeValues), array_values($attributeValues));

        return ($this->owner)::find()->where($condition);
    }

    public function reorder()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function canMoveUp()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function canMoveDown()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function canMoveFirst()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function canMoveLast()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function moveUp()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function moveDown()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function moveFirst()
    {
        throw new NotSupportedException('Not implemented yet');
    }

    public function moveLast()
    {
        throw new NotSupportedException('Not implemented yet');
    }
}